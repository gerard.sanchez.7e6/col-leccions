import java.util.*

/*
* AUTHOR: Gerard Sanchez Preto
* DATE: 23/03/15
* TITLE: GymControlApp
*/
fun main(){
    val scanner = Scanner(System.`in`)
    println("Introdueix els id del usuaris:")

    val gymUsers = mutableSetOf<Int>()

    for (i in 1.. 8){
        val userId = scanner.nextInt()

        if (gymUsers.contains(userId)){
            gymUsers.remove(userId)
            println("$userId SORTIDA")
        }
        else{
            gymUsers.add(userId)
            println("$userId ENTRADA")
        }
    }
}