import java.util.*

/*
* AUTHOR: Gerard Sanchez Preto
* DATE: 23/03/15
* TITLE: RoadSigns
*/
fun main(){
    val scanner = Scanner(System.`in`)
    println("Quants cartells vols introduir?")
    val number = scanner.nextInt()

    val cartells = mutableMapOf<Int,String>()

    println("Introdueix el kilometre i el text associat:")
    for (i in 1..number){
        cartells[scanner.nextInt()] = scanner.nextLine()
    }
    println("Quanes consultes vols fer?")
    val consultes = scanner.nextInt()

    println("Introdueix les consultes:")
    for (i in 1..consultes){
        val eleccio = scanner.nextInt()

        if (eleccio in cartells){
            println(cartells[eleccio])
        }
        else{
            println("No hi ha cartell")
        }
    }
}

