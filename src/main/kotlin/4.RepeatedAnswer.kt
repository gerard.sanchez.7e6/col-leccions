import java.util.*

/*
* AUTHOR: Gerard Sanchez Preto
* DATE: 23/03/15
* TITLE: RepeatedAnswer
*/
fun main(){
    val scanner = Scanner(System.`in`)
    println("Introdueix les respostes:")

    val list = mutableSetOf<String>()

    do{
        val resposta = scanner.next()
        if (resposta.uppercase() != "END") {
            if (list.contains(resposta)) {
                println("MEEEC!")

            } else {
                list.add(resposta)
            }
        }
    }while(resposta.uppercase() != "END")

}