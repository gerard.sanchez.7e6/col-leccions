import java.util.*

/*
* AUTHOR: Gerard Sanchez Preto
* DATE: 23/03/15
* TITLE: SchoolDelegatVoting
*/
fun main(){
    val scanner = Scanner(System.`in`)
    println("Introdueix els vots:")

    val vots = mutableMapOf<String,Int>()

    do{
        val vot = scanner.next()
        if (vot.uppercase() != "END") {
            if (vots.contains(vot)) {
                vots[vot] = vots[vot]!! + 1

            } else {
                vots[vot] = 1
            }
        }
    }while(vot.uppercase() != "END")

    for (key in vots.keys){
        println("$key : ${vots[key]}")

    }


}