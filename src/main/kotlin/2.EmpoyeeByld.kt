import java.util.*

/*
* AUTHOR: Gerard Sanchez Preto
* DATE: 23/03/15
* TITLE: EmpoyeeById
*/

data class Empleat(
    val nom : String,
    val cognom : String,
    val adreca : String)

fun main(){
    val scanner = Scanner(System.`in`)
    println("Quants empleats vols crear?")
    val number = scanner.nextInt()

    val empleats = mutableMapOf<String,Empleat>()

    println("Introdueix Dni, nom ,cognom i adreça:")
    for (i in 1..number){
        val dni = readln()
        val nom = readln()
        val cognom = readln()
        val adreca = readln()

        empleats[dni] = Empleat(nom,cognom,adreca)
    }

    println("Introdueix el dni del empleats que vols trobar:")
    do{
        val userDni = scanner.next().uppercase()
        if (userDni != "END"){
            println("${empleats[userDni]?.nom} ${empleats[userDni]?.cognom} - $userDni, ${empleats[userDni]?.adreca}")
        }

    }while(userDni.uppercase() != "END")

}