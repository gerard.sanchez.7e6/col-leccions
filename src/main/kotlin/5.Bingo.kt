import java.util.*

/*
* AUTHOR: Gerard Sanchez Preto
* DATE: 23/03/15
* TITLE: Bingo
*/
fun main(){
    val scanner = Scanner(System.`in`)
    println("Introdueix els numeros del teu cartró:")

    val cartro = mutableSetOf<Int>()

     for (i in 1.. 10){
         val userNumber = scanner.nextInt()
         cartro.add(userNumber)
     }
    println("Numeros cantats:")
    while(cartro.isNotEmpty()){

        val number = scanner.nextInt()

        if (cartro.contains(number)) {
            cartro.remove(number)

        }
        if(cartro.isNotEmpty()){
            println("Em queden ${cartro.size} números")
        }

    }
    println("BINGO!!")

}